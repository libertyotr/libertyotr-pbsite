% Liberty on the Rocks - Philadelphia


<!---
***This is a subtitle***
**Author:** *Philadelphia*
-->


---

We're a local network of libertarian thinkers who want more than an online conversation.  We aim to gather liberty-loving individuals of all political parties in the pubs, bars and restaurants of Philadelphia to keep the conversation going.  Meetings are suitable for drinkers and non-drinkers alike.  Come enjoy great discussions with others who endeavor to limit the reach of government into their personal lives and further the cause of liberty.

It's FREE to join our group

<br>

## Events

We aim for two events each month.

<br>

### Second Tuesdays

Every second Tuesday we meet in center city. The Irish Pub on 20th St is our regular spot. This is a social occasion for old and new friends to get together over cold drinks and discuss the issues.

### Fourth Tuesdays

Every fourth Tuesday we meet outside center city. We mix up the venues to keep things interesting and explore new neighborhood bars.


<br>
<br>



Links:

[libertyotr.eventbrite.com](https://libertyotr.eventbrite.com)

[Google Calendar](https://calendar.google.com/calendar?cid=cGhpbGFkZWxwaGlhQGxpYmVydHlvbnRoZXJvY2tzLm9yZw)

[libertyontherocks.org](https://libertyontherocks.org)

---

